'use strict';
var fs = require('fs');
var path = require('path');
var cheerio = require('cheerio');

function main(html) {
  var $ = cheerio.load(html);
  var ports = $('table.wikitable > tr').map(function(i, el) {
    var $tr = $(el);
    var $td = $tr.find('td');

    if($td.length === 0) {
      return null;
    }

    var port = +$($td.get(0)).text();
    var protocol = $($td.get(1)).text();
    var description = $($td.get(3)).text();

    return {
      port: port,
      protocol: protocol,
      description: description,
    };
  }).get();

  var newPorts = [];
  for(var i = 0; i < ports.length; i++) {
    if(ports[i].port !== NaN) {
      newPorts.push(ports[i]);
    }
  }

  console.log(JSON.stringify(newPorts));
}

fs.readFile(path.join(__dirname, 'portmap.html'), function(err, buffer) {
  if(err) {
    console.error(err.message);
    process.exit(1);
  }

  main(buffer.toString());
});
