portmap
=======
Scraped port to service data from https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

```
npm install portmap
```

## License
This code is licensed under the MIT license.

## Donations
Would you like to buy me a beer? Send bitcoin to 3JjxJydvoJjTrhLL86LGMc8cNB16pTAF3y
